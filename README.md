# Space Invaders in LUA

A space invaders clone written in LUA using the Love2d framework.

##Setup:

Download Love2d:
https://love2d.org/

Get a good Text Editor/IDE that supports Love2d.
(I use Atom Editor with the love-ide addon).
