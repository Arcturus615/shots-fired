local path = 'src/states/menu_state'

local function load()
end

local function draw(self)
end

local function update(self)
end

return{
  path=path,
  
  load=load,
  draw=draw,
  update=update,
}