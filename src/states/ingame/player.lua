local path = '/src/states/ingame/'

local color = require(LIBS..'color')
local vector = require(LIBS..'vector')
local hitbox = require(LIBS..'hitbox')
local graphic = require(LIBS..'graphic')
local laser = require(path..'laser')
local clamp = math.clamp

local Player = {}
Player.__type = 'player'
Player.__index = Player
Player.i = 1

function Player.init(x, y, w, h, s)
  return setmetatable(
    {
      shottimer = 0,
      spd = s,
      
      pos = vector( (x or 0), (y or 0) ),
      vel = vector(0,0),
      acc = vector(0,0), 

      box = hitbox(
        (x or 0),
        (y or 0),
        (w or 32),
        (h or 16)
        ),
    },
    Player)
end

function Player:update(dt, game)
  if self.laserfire then
   local live = self.laserfire:update(dt, game)
   if not live then self.laserfire = false end
  end

  self:move(dt)

  self.vel = self.vel + self.acc
  self.pos = self.pos + self.vel

  self.pos.x = clamp(game.area.x, self.pos.x, (game.area.x + game.area.w) - self.box.w)

  self.box.x = self.pos.x
  self.box.y = self.pos.y
end

function Player:draw()
  if self.laserfire then
    self.laserfire:draw()
  end
  graphic.rectFill(
    color.WHITE,
    self.box.x,
    self.box.y,
    self.box.w,
    self.box.h
  )
end

-- 
-- Movement
-- 

function Player:left(dt)
  self.vel.x = -5
end 

function Player:right(dt)
  self.vel.x = 5
end

function Player:halt()
  self.vel.x = 0
  self.acc.x = 0
end

function Player:shoot(dt,map)
  if not self.laserfire then
    self.laserfire = 
      laser.fire(
        'invader',
        self.pos.x + self.box.w / 2,
        self.pos.y - self.box.h / 2,
        0, UP)
  end
end

function Player:move(dt, map)
  local down = love.keyboard.isDown
  if down('space') then self:shoot(dt) end
  
  if down('left') then self:left(dt)
  elseif down('right') then self:right(dt)
  else
    self:halt()
  end
end

function Player:shot(game)
  game.lives = game.lives - 1
  game:restartPlayer()
end

return Player