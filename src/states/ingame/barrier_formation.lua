local Formation = {}
Formation.__index = Formation
Formation.__type = 'formation'

local hitbox = require(LIBS..'hitbox')
local graphic = require(LIBS..'graphic')
local color = require(LIBS..'color') 

local function _spawnBarriers(arena, count)
	local structure = {
		[1] = {5,5,5,5,5,5},
		[2] = {5,5,5,5,5,5},
		[3] = {5,5,0,0,5,5}
		}

	local w, h = love.graphics.getDimensions()
	local bw, bh = 16, 12

	local _sections = {}
	for i=1, count do
		table.insert(
			_sections,
			hitbox(
				((arena.w / (count)) * (i - 1)) + bw*3,
        (arena.h - arena.h / 7),
        bw * 6,
        bh * 6
			)
		)
  end
  return _sections
end

function Formation:update(dt, game)
end

function Formation.form(arena, count)
  return setmetatable(
    {
      sections = _spawnBarriers(arena, count)
    },
    Formation)
end

function Formation:draw()
  for _, section in ipairs(self.sections) do
    graphic.rectLine(
      color.GREEN,
      section.x,
      section.y,
      section.w,
      section.h,
      2
    )
  end
end

return Formation