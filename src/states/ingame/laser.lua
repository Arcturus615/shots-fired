local path = '/src/states/ingame/'

local color = require(LIBS..'color')
local vector = require(LIBS..'vector')
local hitbox = require(LIBS..'hitbox')
local graphic = require(LIBS..'graphic')

local Laser = {}
Laser.__type = 'laser'
Laser.__index = Laser

function Laser:collide(other_entity, game)
  if other_entity:shot(game) then
    self.live = false
    return true
  end

  return false
end

function Laser.fire(target, sx,sy,dx,dy, w,h, laserColor)
  return setmetatable(
    {
      color = (laserColor or color.RED),
      target = target,
      live = true,
      acc = vector(0,0),
      vel = vector( 0, 10 * dy ),
      pos = vector( (sx or 0), (sy or 0) ),
      box = hitbox(
        (sx or 0),
        (sy or 0),
        (w or 4),
        (h or 8)
      )
    }, Laser)
end

function Laser:update(dt, game)
  local winW = love.graphics.getWidth()
  local winH = love.graphics.getHeight()

  self.vel = self.vel + self.acc
  self.pos = self.pos + self.vel
  self.box.x = self.pos.x
  self.box.y = self.pos.y

  if not game:checkCollision(self) then
    if not self.live
    or self.pos.x - self.box.w < 0
    or self.pos.x + self.box.w > winW
    or self.pos.y - self.box.h < 0
    or self.pos.y + self.box.h > winH
    then
      return false
    end
  end

  return true
end

function Laser:draw()
  if self.live then
    graphic.rectFill(
      self.color,
      self.box.x,
      self.box.y,
      self.box.w,
      self.box.h
    )
  end
end

return Laser