local path = '/src/states/ingame/'

local color = require(LIBS..'color')
local vector = require(LIBS..'vector')
local hitbox = require(LIBS..'hitbox')
local graphic = require(LIBS..'graphic')
local laser = require(path..'laser')

local clamp = math.clamp
local rand = math.random

local Invader = {}
Invader.__index = Invader
Invader.__type = 'invader'

Invader.possibleLaserColors = {
  color.RED,
  color.BLUE,
  color.GREEN,
  color.YELLOW,
  color.PURPLE
}

local function _drawDebugInfo(self)
  if gamestate.debugmode then
    graphic.line(
    self.debugcolor,
    self.box.x + self.box.w + 10,
    self.box.y - 5,
    self.box.x + self.box.w + 10,
    self.box.y + self.box.h + 5,
    1
    )
    graphic.text(
      math.floor(self.maxshottimer),
      self.debugcolor,
      self.box.x + self.box.w + 10 + 5,
      self.box.y - 5
    )
    graphic.text(
      math.floor(self.shottimer),
      self.debugcolor,
      self.box.x + self.box.w + 10 + 5,
      self.box.y + 10
    )
  end
end

function Invader.init(id, x, y, hp, points)
  local _maxshotimer = rand(15, 60)
  local _shottimer = clamp(
        5,
        rand(rand(10) + rand(15) + rand(20)),
        _maxshotimer - 3)
  return setmetatable(
    {
      hp = (hp or 1),
      color = color.WHITE,
      points = (points or 25),
      debugcolor = color.WHITE,
      dir = 1,
      acc = vector(),
      vel = vector(),
      pos = vector( (x or 0), (y or 0) ),
      box = hitbox(
        (x or 0),
        (y or 0),
        (w or 24),
        (h or 24)
      ),
      id = (id or 0),
      time = 0,
      rtime = 0,
      shottimer = _shottimer,
      maxshottimer = _maxshotimer
    },
    Invader)
end

function Invader:update(dt, game, formation, x, y)
  self.time = self.time + dt

  self.pos.x = x
  self.pos.y = y

  self.box.x = x
  self.box.y = y

  if self.hp > 0 then
    if not self.laserfire then
      if self.shottimer > self.maxshottimer then
        self.debugcolor = color.RED
        self.color = color.RED
        if rand(100) == 100 then
          self:fireLaser()
          self.debugcolor = color.WHITE
          self.color = color.WHITE
          self.shottimer = rand( self.maxshottimer )
          if self.shottimer == self.maxshottimer then
            self.shottimer = self.shottimer - rand(2)
          end
        end
      else
        self.shottimer = self.shottimer + dt
      end
    end

    if self.laserfire then
      local live = self.laserfire:update(dt, game)
      if not live then self.laserfire = false
      end
    end
    
  end
end

function Invader:draw()
  if self.hp > 0 then
    if self.laserfire then
      self.laserfire:draw()
    end

    graphic.rectFill(
      self.color,
      self.box.x,
      self.box.y,
      self.box.w,
      self.box.h
    )

    -- _drawDebugInfo(self)
  end
end

function Invader:shot(game, formation)
  if self.hp > 0 then
    self.hp = self.hp - 1
    if self.hp <= 0 then
      game.points = game.points + self.points
      if HISCORE < game.points then HISCORE = game.points end
      game.formation:invaderDead(self.id, game)
    end
    return true
  end

  return false
end

function Invader:fireLaser()
  if not self.laser then
    self.laserfire = laser.fire(
      'player',
      self.pos.x + self.box.w / 2,
      self.pos.y + self.box.h / 2,
      0, DOWN, nil, nil, self.possibleLaserColors[math.random(5)]
    )
  end
end

return Invader