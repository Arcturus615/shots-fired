local path = '/src/states/ingame/'

local box = require(LIBS..'hitbox')
local vector = require(LIBS..'vector')
local graphic = require(LIBS..'graphic')
local color = require(LIBS..'color')

local clamp = math.clamp

local Formation = {}
Formation.__index = Formation

local function _spawnInvaders(rect, invadersW, invadersH)
	local winw, winh = love.graphics.getDimensions()
	local _invaders = {}

  for h=1, invadersH do
    if not _invaders[h] then _invaders[h] = {} end
    for w=1, invadersW do
			table.insert(
				_invaders[h],
				require(path..'invader').init(
          rect.x + (rect.w / invadersW) * (w - 1) + 8,
          rect.y + (rect.h / invadersH) * (h - 1) + 8
        )
      )
		end
  end
  
  _invaders.count = invadersH*invadersW
  return _invaders
end

function Formation.form(gameArea, invW, invH)
  local invW = (invW or 11)
  local invH = (invH or 5)
  local a = gameArea
  local fw = 16 * invW * 2.5
  local fh = 16 * invH * 2.5
  local fx = a.w / 3
  local fy = a.h / 5
  local _formationbox = box( fx, fy, fw, fh )

  return setmetatable(
  {
    time = 0,
    dir = RIGHT,
    box = _formationbox,
    entities = _spawnInvaders(_formationbox, invW, invH)
  }, 
  Formation)
end

local function _updateInvaders(self, dt, game)
  local box = self.box
  for h, _ in ipairs(self.entities) do
    for w, invader in ipairs(self.entities[h]) do
      invader:update(
        dt, game, self,
        box.x + (box.w / #self.entities[h]) * (w - 1) + 8,
        box.y + (box.h / #self.entities) * (h - 1) + 8
      )
    end
  end
end

function Formation:update(dt, game)
  self.leftbound = game.area.x + 16
  self.rightbound = ((game.area.x + game.area.w) - 16) - self.box.w

  _updateInvaders(self, dt, game)

  if self.time >= 1 then
    self.time = 0
    self.box.x = self.box.x + (8 * self.dir)
    self.box.x = clamp(self.leftbound, self.box.x, self.rightbound)
    if self.box.x <= self.leftbound
    or self.box.x >= self.rightbound
    then
      self.box.y = self.box.y + 16
      self.dir = -(self.dir)
    end
    
  else
    self.time = self.time + dt
  end
  
end

local function _drawDebugInfo(self)
	if gamestate.debugmode then
		graphic.rectLine(
			color.RED,
			self.box.x,
			self.box.y,
			self.box.w,
			self.box.h,
			2
		)
	end
end

function Formation:draw()
  local box = self.box
  for h, htable in ipairs(self.entities) do
    for w, invader in ipairs(self.entities[h]) do
      invader:draw()
    end
  end
  _drawDebugInfo(self)
end

function Formation:invaderDead(id, game)
  self.entities.count = self.entities.count - 1
  if self.entities.count <= 0 then
    game:newWave()
  end
end

return Formation