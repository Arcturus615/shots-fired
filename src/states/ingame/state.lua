local path = '/src/states/ingame/'

local box = require(LIBS..'hitbox')
local graphic = require(LIBS..'graphic')
local color = require(LIBS..'color')
local down = love.keyboard.isDown

local Game = {}
Game.__index = Game

local function _spawnPlayer()
	local winw, winh = love.graphics.getDimensions()
	return require(path..'player').init(winw / 2, winh - (winh / 20))
end

local function _initGameArea()
	local winw, winh = love.graphics.getDimensions()
	return box(
		0,
		winh/ 10,
		winw,
		winh - (winh / 10))
end

function Game.init()
	local entities = {}
	local _area = _initGameArea()
	local _formation = require(path..'invader_formation').form(_area)
	local _player = _spawnPlayer(_area)
	local _barriers = require(path..'barrier_formation').form(_area, 4)
	return setmetatable(
		{
			time = 0,
			lives = 3,
			points = 0,

			area = _area,
			player = _player,
			barriers = _barriers,
			formation = _formation,
		},
		Game)
end

function Game:load()
	function love.keypressed(key)
		if key == 'return' then
			gamestate:load('pause')
		elseif key == 'f10' then
			gamestate:toggleDebug()
		elseif key == 'q' and gamestate.debugmode then
			self:gameover()
		end
	end
end

function Game:update(dt, gamestate)
	self.time = self.time + dt
	if not self.formation then
		self.formation = require(path..'invader_formation').form(self.area)
	end
	self.formation:update(dt, self)
	self.player:update(dt, self)
end

local function _drawHUD(self)
	local line = 2
	graphic.rectLine(
		color.BLUE,
		self.area.x + line/2,
		self.area.y,
		self.area.w - (line),
		self.area.h - (line),
		line
	)
	graphic.text(
		'Lives: '..self.lives,
		color.WHITE,
		(self.area.x + self.area.w) - 85,
		self.area.y - 25,
		'left', 2000
	)
	graphic.text(
		'Time: '..math.floor(self.time),
		color.WHITE,
		(self.area.x + self.area.w) - 85,
		self.area.y - 50,
		'left', 2000
	)
	graphic.text(
		'Score: '..self.points,
		color.WHITE,
		25,
		self.area.y - 25,
		'left', 2000
	)
	graphic.text(
		'High Score: '..HISCORE,
		color.WHITE,
		25,
		self.area.y - 50,
		'left', 2000
	)
end

function Game:draw()
	if self.formation then self.formation:draw() end
	if self.barriers then self.barriers:draw() end
	self.player:draw()

	_drawHUD(self)
end

function Game:checkCollision(laser)
	if laser.target == 'player' then
		if laser.box:checkCollision(self.player.box) then
			laser:collide(self.player, self)
		end
	end
	
	if laser.target == 'invader' then
		for h, _ in ipairs(self.formation.entities) do
			for w, invader in ipairs(self.formation.entities[h]) do
				if laser.box:checkCollision(invader.box) then
					laser:collide(invader, self)
					return true
				end
			end
		end
	end
end

function Game:newWave()
	self.lives = self.lives + 1
	self.player.laserfire = nil
	self.formation = false
end

function Game:restartPlayer()
	if self.lives >= 0 then
		self.player = nil
		self.player = _spawnPlayer()
	else
		self:gameover()
	end
end

function Game:gameover()
	gamestate:load('gameover')
end

function Game:win()
	gamestate:load('win')
end

return Game