local path = 'src/states/ingame/'
local color = require(LIBS..'color')
local hitbox = require(LIBS..'hitbox')
local graphic = require(LIBS..'graphic')

local Barrier = {}
Barrier.__type = 'barrier'

function Barrier.init(hp, x, y, w, h)
  assert(hp and x and y)
  return setmetatable(
    {
      color = color.GREEN,
      hp = hp,
      box = hitbox(
        x,
        y,
        (w or 8),
        (h or 8)
      )
    },
    Barrier)
end

function Barrier:update()
end

function Barrier:draw()
  graphic.rectFill(
    self.color,
    self.box.x,
    self.box.y,
    self.box.w,
    self.box.h
  )
end

return Barrier