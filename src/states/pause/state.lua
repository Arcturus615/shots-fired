local path = '/src/states/pause_state'

local color = require(LIBS..'color')
local graphic = require(LIBS..'graphic')
local down = love.keyboard.isDown

local function update() end

local function load()
  function love.keypressed(key)
    if key == 'return' then
      gamestate:load('ingame')
    end
  end
end

local function draw(self, gamestate)
  local w, h = love.graphics.getDimensions()
  gamestate.states.ingame:draw()
  graphic.rectFill(color.GRAY50,0,0,w,h)
end

return{
  path=path,
  
  load=load,
  draw=draw,
  update=update
}