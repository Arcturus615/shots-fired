local path = '/src/states/gameover'

local color = require(LIBS..'color')
local graphic = require(LIBS..'graphic')
local down = love.keyboard.isDown

local function load()
  function love.keypressed(key, isrepeat)
    if key == 'return' then
      gamestate:reload('ingame')
    end
  end
end

local function update(self, dt, gamestate)
end

local function draw(self, gamestate)
  local w, h = love.graphics.getDimensions()
  gamestate.states.ingame:draw()
  graphic.rectFill(color.BLACK,0,0,w,h)
  graphic.text(
    'Game Over!!\n\nPress [ENTER] to play again!',
    color.WHITE,
    0,
    h / 2.5,
    'center',
    w
  )

end

return{
  path=path,

  load=load,
  draw=draw,
  update=update
}