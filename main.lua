LIBS='src/common/'

HISCORE = 0
UP = -1
DOWN = 1
LEFT = -1
RIGHT = 1

require('src/utils')

function love.load()
	math.initRandom()
	gamestate = require('src/gamestate')
	gamestate:load('ingame')
end

function love.update(dt)
	gamestate:update(dt)
end

function love.draw()
	gamestate:draw()
end
